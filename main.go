package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"golang.org/x/sync/errgroup"
)

type Worker func(ctx context.Context) func() error

var errSignal = errors.New("Received terminating signal")

// waitForSignal _MUST_ return an err on received signal
// that is the only way for the errgroup to start returning
// once a signal has been reached
func waitForSignal(ctx context.Context) func() error {
	return func() error {
		sigs := make(chan os.Signal)

		signal.Notify(sigs, syscall.SIGTERM, syscall.SIGINT)
		defer signal.Stop(sigs)

		select {
		case <-ctx.Done():
			return nil
		case sig := <-sigs:
			return fmt.Errorf("%w: %+v", errSignal, sig)
		}
	}
}

func healthcheck(w http.ResponseWriter, _ *http.Request) {
	// OWASP suggestions
	w.Header().Set("X-Frame-Options", "DENY")
	w.Header().Set("Content-Security-Policy", "default-src 'none';frame-ancestors 'none';form-action 'none'")
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("X-Content-Type-Options", "nosniff")

	w.Header().Set("Content-Type", "text/html")
	if _, err := w.Write([]byte("<!DOCTYPE html><html><head><title>Hello, World!</title></head><body>Hello, World!</body></html>")); err != nil {
		log.Printf("Failed in healthcheck: %+v", err)
		return
	}
}

func healthcheckServer(ctx context.Context) func() error {
	return func() error {
		http.HandleFunc("/", healthcheck)
		srv := &http.Server{Addr: ":5000"}

		go func() {
			<-ctx.Done()
			// we want a _NEW_ context here intentionally
			// because our "normal" one is already closed at this point
			shutdownCtx, shutdownCancel := context.WithTimeout(context.Background(), 2*time.Second)
			defer shutdownCancel()

			if err := srv.Shutdown(shutdownCtx); errors.Is(err, context.Canceled) {
				log.Printf("Failed to gracefully shut down HTTP in time; killing current requests")
				if err := srv.Close(); err != nil {
					log.Printf("Err kililng current requests: %+v", err)
				}
			} else if err != nil {
				log.Printf("Err shutting down HTTP server: %+v", err)
			}
		}()

		// XXX: What if Shutdown() fails to Shutdown?
		// will our function never return?
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			return err
		}

		return nil
	}
}

func automata(ctx context.Context) func() error {
	return func() error {
		t := time.NewTicker(5 * time.Minute)
		defer t.Stop()

		for {
			select {
			case <-ctx.Done():
				return nil
			case <-t.C:
				// TODO
			}
		}
	}
}

func main() {
	log.Printf("Hello, World!")

	workers := []Worker{waitForSignal, healthcheckServer, automata}

	grp, ctx := errgroup.WithContext(context.Background())

	for _, w := range workers {
		grp.Go(w(ctx))
	}

	// wait until all of our goroutines have returned
	if err := grp.Wait(); err != nil && !errors.Is(err, errSignal) {
		// we defer the Fatal because Fatal will call os.Exit(1)
		// and prevent other defer's from running
		defer log.Fatal(err)
	}

	log.Printf("Goodbye, World!")
}
